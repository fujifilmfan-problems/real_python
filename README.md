## Python Practice Problems

### References
---
* [Real Python "Python Practice Problems: Get Ready for Your Next Interview"](https://realpython.com/python-practice-problems/)
* [Real Python GitHub source files](https://github.com/realpython/materials/tree/master/python-practice-problems)

### Project Set Up
---
This project uses `Poetry` and `venv`.  
`$ poetry new practice_problems`  
`$ poetry install --no-root`  
`$ source .venv/bin/activate` (so I don't need `poetry run`)  

### Notes
---

#### Python Practice Problem 1: Sum of a Range of Integers
The tests in the GitHub repo expect a negative input to result in `0`, but this is because the solution uses `range`, 
and `range` has a default starting value of 0.  There are no values in the range from zero to a negative number 
(although we could adjust the step to a negative number if we wanted a little added complexity).

Since the enhanced function, which accepts a low number and a high number, the ranges are valid as long as the
low number is less than or equal to the high one.  Accordingly, this function sums negative numbers without returning 
zero.

#### Python Practice Problem 2: Caesar Cipher / Python Practice Problem 3: Caesar Cipher Redux
TODO: Write a solution using a list comprehension.

#### Python Practice Problem 4: Log Parser
As this task seemed a bit more complicated than previous ones, I split my `main` function into several dependent 
functions.  I wrote very simple unit tests for this module, but they only cover ideal cases.  Adding the logical 
complexity to deal with cases other than those presented by the sample log file would go beyond the scope of this 
practice problem.

A couple things that the Real Python solution did that mine didn't were the use of a generator for reading the log file 
and keeping track of the state of machine in a variable.  I especially like that latter one as my own method was a bit 
messy and two verbose.

Unfortunately, Real Python's time handling rounds the uptime to 7 s when it's really ~6.31 s because `time.strptime` 
does not consider microseconds.

#### Python Practice Problem 5: Sudoku Solver
This is a little involved for the moment, but sounds like fun!
