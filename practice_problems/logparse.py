#!/usr/bin/env python

""" log parser
    Accepts a filename on the command line. The file is a Linux-like log file
    from a system you are debugging. Mixed in among the various statements are
    messages indicating the state of the device. They look like this:
        Jul 11 16:11:51:490 [139681125603136] dut: Device State: ON
    The device state message has many possible values, but this program cares
    about only three: ON, OFF, and ERR.

    Your program will parse the given log file and print out a report giving
    how long the device was ON and the timestamp of any ERR conditions.
"""

import argparse
from datetime import datetime
from pathlib import Path


def return_parsed_args(arguments):
    """Parse and define command line arguments.

    :param arguments: LIST; like ['tests/test.log']
    :return: OBJ; Namespace object looking something like this:
        Namespace(file='tests/test.log')
    """

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('file', type=str, help="""Name of log file to parse.""")

    return parser.parse_args(arguments)


def parse_log_for_events(file):
    """Parse log for on, off, and error events.

    :param file: OBJ; Path object containing file name to find
    :return: DICT; contains on, off, and error timestamps as strings
    """

    events = {
        'errors': [],
        'on_off': [],
    }

    with open(file) as f:
        for line in f:
            if 'Device State: ERR' in line:
                events['errors'].append(line)
            elif 'Device State: ON' in line:
                events['on_off'].append({'on': line})
            elif 'Device State: OFF' in line:
                events['on_off'].append({'off': line})

    return events


def calculate_uptime(on_off):
    """Calculate total uptime from on and off timestamps.

    :param on_off: LIST; contains dicts like {'on': 'timestamp'}
    :return: INT
    """

    if len(on_off) % 2 is not 0:
        on_off.append(None)

    total_uptime = 0

    paired_timestamps = zip(on_off[0::2], on_off[1::2])
    for pair in paired_timestamps:
        on = pair[0]['on'][7:19]
        on_datetime_obj = datetime.strptime(on, '%H:%M:%S:%f')
        off = pair[1]['off'][7:19]
        off_datetime_obj = datetime.strptime(off, '%H:%M:%S:%f')
        delta = off_datetime_obj - on_datetime_obj
        total_uptime += delta.total_seconds()

    return total_uptime


def assemble_message(uptime, errors):
    """Assemble message string.

    :param uptime: FLOAT; time in seconds
    :param errors: LIST; timestamp strings
    :return: STR; final message
    """

    message_pieces = [
        f'Device was on for {round(uptime, 2)} seconds',
        'Timestamps of error events:',
    ]
    for error in errors:
        message_pieces.append(error)
    message = '\n'.join(message_pieces)

    print(message)
    return message


def main(cli_args):
    package_dir = Path(__file__).absolute().parent.parent
    file = Path.joinpath(package_dir, cli_args.file)

    events = parse_log_for_events(file)
    uptime = calculate_uptime(events['on_off'])
    errors = [timestamp[:19] for timestamp in events['errors']]

    message = assemble_message(uptime, errors)

    return message


if __name__ == '__main__':
    import sys
    args = return_parsed_args(sys.argv[1:])
    main(args)
