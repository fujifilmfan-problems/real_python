#!/usr/bin/env python

import string


""" Caesar Cipher
    A Caesar cipher is a simple substitution cipher in which each letter of the
    plain text is substituted with a letter found by moving n places down the
    alphabet. For example, assume the input plain text is the following:

        abcd xyz

    If the shift value, n, is 4, then the encrypted text would be the following:

        efgh bcd

    You are to write a function that accepts two arguments, a plain-text
    message and a number of letters to shift in the cipher. The function will
    return an encrypted string with all letters transformed and all
    punctuation and whitespace remaining unchanged.

    Note: You can assume the plain text is all lowercase ASCII except for
    whitespace and punctuation.
"""


def caesar(plain_text, shift_num):
    alphabet = string.ascii_lowercase
    new_sequence = []
    for char in plain_text:
        if char in alphabet:
            char_index = alphabet.index(char)
            fetch = (char_index + shift_num) % 26
            new_sequence.append(alphabet[fetch])
        else:
            new_sequence.append(char)
    return ''.join(new_sequence)


def caesar_using_translate(plain_text, shift_num):
    in_table = string.ascii_lowercase
    out_table = ''.join([in_table[shift_num:], in_table[:shift_num]])
    trans_table = str.maketrans(in_table, out_table)

    return plain_text.translate(trans_table)


caesar_using_translate('aaa.bbb', -1)
