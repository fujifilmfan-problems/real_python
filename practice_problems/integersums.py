#!/usr/bin/env python

""" Sum of Integers Up To n
    Write a function, add_it_up(), that takes a single integer as input
    and returns the sum of the integers from zero to the input parameter
    (inclusive).

    The function should return 0 if a non-integer is passed in.
"""


def add_it_up(n):
    sum_ = 0

    try:
        for num in range(n+1):
            sum_ += num
    except TypeError:
        return 0

    return sum_


def better_add_it_up(n):
    try:
        sum_ = sum(range(n + 1))
    except TypeError:
        sum_ = 0

    return sum_


def add_it_up_enhanced(low, high):
    try:
        sum_ = sum(range(low, high + 1))
    except TypeError:
        sum_ = 0

    return sum_
