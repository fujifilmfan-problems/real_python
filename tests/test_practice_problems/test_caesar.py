#!/usr/bin/env python

import pytest

from practice_problems import caesar


@pytest.mark.parametrize('string, shift, expected', [
    ('aaa', 1, 'bbb'), ('aaa', 5, 'fff')
])
def test_a(string, shift, expected):
    actual = caesar.caesar(string, shift)
    assert actual == expected


@pytest.mark.parametrize('string, shift, expected', [
    ('aaa.bbb', 1, 'bbb.ccc'), ('aaa.bbb', -1, 'zzz.aaa')
])
def test_punctuation(string, shift, expected):
    actual = caesar.caesar(string, shift)
    assert actual == expected


@pytest.mark.parametrize('string, shift, expected', [
    ('aaa    bb b', 1, 'bbb    cc c'), ('aaa    bb b', 3, 'ddd    ee e')
])
def test_whitespace(string, shift, expected):
    actual = caesar.caesar(string, shift)
    assert actual == expected


@pytest.mark.parametrize('string, shift, expected', [
    ('abc', -1, 'zab'),
    ('abc', -2, 'yza'),
    ('abc', -3, 'xyz'),
    ('xyz', 1, 'yza'),
    ('xyz', 2, 'zab'),
    ('xyz', 3, 'abc'),
])
def test_wraparound(string, shift, expected):
    actual = caesar.caesar(string, shift)
    assert actual == expected


@pytest.mark.parametrize('string, shift, expected', [
    ('aaa', 1, 'bbb'), ('aaa', 5, 'fff')
])
def test_a_with_translate(string, shift, expected):
    actual = caesar.caesar_using_translate(string, shift)
    assert actual == expected


@pytest.mark.parametrize('string, shift, expected', [
    ('aaa.bbb', 1, 'bbb.ccc'), ('aaa.bbb', -1, 'zzz.aaa')
])
def test_punctuation_with_translate(string, shift, expected):
    actual = caesar.caesar_using_translate(string, shift)
    assert actual == expected


@pytest.mark.parametrize('string, shift, expected', [
    ('aaa    bb b', 1, 'bbb    cc c'), ('aaa    bb b', 3, 'ddd    ee e')
])
def test_whitespace_with_translate(string, shift, expected):
    actual = caesar.caesar_using_translate(string, shift)
    assert actual == expected


@pytest.mark.parametrize('string, shift, expected', [
    ('abc', -1, 'zab'),
    ('abc', -2, 'yza'),
    ('abc', -3, 'xyz'),
    ('xyz', 1, 'yza'),
    ('xyz', 2, 'zab'),
    ('xyz', 3, 'abc'),
])
def test_wraparound_with_translate(string, shift, expected):
    actual = caesar.caesar_using_translate(string, shift)
    assert actual == expected
