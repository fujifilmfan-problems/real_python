#!/usr/bin/env python

import argparse
from pathlib import Path

from practice_problems import logparse

TEST_FILE = 'tests/test_practice_problems/test.log'


def test_return_parsed_args():
    # Arrange
    arguments = [TEST_FILE]
    parser = argparse.ArgumentParser()
    parser.add_argument('file')
    expected = parser.parse_args(arguments)

    # Act
    actual = logparse.return_parsed_args(arguments)

    # Assert
    assert actual == expected


def test_parse_log_for_events():
    # Arrange
    package_dir = Path(__file__).absolute().parent.parent.parent
    file = Path.joinpath(package_dir, TEST_FILE)
    expected = {
        'errors': ['Jul 11 16:11:54:661 [139681125603136] dut: Device State: ERR\n',
                   'Jul 11 16:11:56:067 [139681125603136] dut: Device State: ERR\n'],
        'on_off': [
            {'on': 'Jul 11 16:11:51:490 [139681125603136] dut: Device State: ON\n'},
            {'off': 'Jul 11 16:11:53:490 [139681125603136] dut: Device State: OFF\n'},
            {'on': 'Jul 11 16:11:53:490 [139681125603136] dut: Device State: ON\n'},
            {'off': 'Jul 11 16:11:57:305 [139681125603136] dut: Device State: OFF\n'},
            {'on': 'Jul 11 16:11:58:710 [139681125603136] dut: Device State: ON\n'},
            {'off': 'Jul 11 16:11:59:201 [139681125603136] dut: Device State: OFF\n'}
        ],
    }

    # Act
    actual = logparse.parse_log_for_events(file)

    # Assert
    assert actual == expected


def test_calculate_uptime():
    # Arrange
    on_off = [{'on': 'Jul 11 16:11:51:490'}, {'off': 'Jul 11 16:11:53:490'}]
    expected = 2.0

    # Act
    actual = logparse.calculate_uptime(on_off)

    # Assert
    assert actual == expected


def test_assemble_message():
    # Arrange
    uptime = 6.4567
    errors = ['Jul 11 16:11:54:661', 'Jul 11 16:11:56:067']
    expected = '\n'.join([
        'Device was on for 6.46 seconds',
        'Timestamps of error events:',
        'Jul 11 16:11:54:661',
        'Jul 11 16:11:56:067',
    ])

    # Act
    actual = logparse.assemble_message(uptime, errors)

    # Assemble
    assert actual == expected


def test_main():
    # Note: without mocks, this test also implicitly tests dependent
    # functions.

    # Arrange
    args = logparse.return_parsed_args([TEST_FILE])
    expected = '\n'.join([
        'Device was on for 6.31 seconds',
        'Timestamps of error events:',
        'Jul 11 16:11:54:661',
        'Jul 11 16:11:56:067'
    ])

    # Act
    actual = logparse.main(args)

    # Assert
    assert actual == expected
