#!/usr/bin/env python

from practice_problems import __version__


def test_version():
    assert __version__ == '0.1.0'
