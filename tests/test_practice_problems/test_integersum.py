#!/usr/bin/env python

import pytest

from practice_problems import integersums


def test_add_it_up_ints_to_ten():
    results = [0, 1, 3, 6, 10, 15, 21, 28, 36, 45]
    for n in range(10):
        actual = integersums.add_it_up(n)
        expected = results[n]
        assert actual == expected


def test_add_it_up_negative():
    actual = integersums.add_it_up(-7)
    expected = 0
    assert actual == expected


def test_add_it_up_float():
    actual = integersums.add_it_up(0.123)
    expected = 0
    assert actual == expected


def test_add_it_up_string():
    actual = integersums.add_it_up('string')
    expected = 0
    assert actual == expected


def test_better_add_it_up_ints_to_ten():
    results = [0, 1, 3, 6, 10, 15, 21, 28, 36, 45]
    for n in range(10):
        actual = integersums.better_add_it_up(n)
        expected = results[n]
        assert actual == expected


def test_better_add_it_up_negative():
    actual = integersums.better_add_it_up(-7)
    expected = 0
    assert actual == expected


def test_better_add_it_up_float():
    actual = integersums.better_add_it_up(0.123)
    expected = 0
    assert actual == expected


def test_better_add_it_up_string():
    actual = integersums.better_add_it_up('string')
    expected = 0
    assert actual == expected


@pytest.mark.parametrize('low, high, expected', [
    (0, 5, 15), (6, 9, 30), (3, 1, 0), (3, 3, 3)
])
def test_add_it_up_enhanced_positive(low, high, expected):
    actual = integersums.add_it_up_enhanced(low, high)
    assert actual == expected


@pytest.mark.parametrize('low, high, expected', [
    (-5, 5, 0), (-5, 0, -15), (-5, 9, 30), (2, -7, 0), (-5, -5, -5)
])
def test_add_it_up_enhanced_negative(low, high, expected):
    actual = integersums.add_it_up_enhanced(low, high)
    assert actual == expected


@pytest.mark.parametrize('low, high', [
    (0.123, 9), (0, .123)
])
def test_add_it_up_enhanced_float(low, high):
    actual = integersums.add_it_up_enhanced(low, high)
    expected = 0
    assert actual == expected


@pytest.mark.parametrize('low, high', [
    (0, 'string'), ('string', 9)
])
def test_add_it_up_enhanced_string(low, high):
    actual = integersums.add_it_up_enhanced(low, high)
    expected = 0
    assert actual == expected
